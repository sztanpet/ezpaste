<?php
interface passwordGenerator {
  public static function getPassword( $length );
}

class PhoneticPassword implements passwordGenerator {
  protected static $consonants = array(
    'b', 'c', 'd', 'f', 'g',
    'h', 'j', 'k', 'l', 'm',
    'n', 'p', 'q', 'r', 's',
    't', 'v', 'w', 'x', 'y',
  );
  protected static $vowels = array(
    'a', 'e', 'i', 'o', 'u',
  );
  
  public static function getPassword( $length ) {
    
    $password = array();
    for ( $i=0; $i < $length; $i++ ) { 
      
      if ( $i % 2 == 0 )
        $password[] = self::$consonants[ array_rand( self::$consonants ) ];
      else
        $password[] = self::$vowels[ array_rand( self::$vowels ) ];
      
    }
    
    return implode( $password );
    
  }
  
}

class DB {
  protected $db;
  protected $prefix = 'ezp-';
  protected $keylength = 6;
  
  public function __construct( $config ) {
    
    foreach( $config as $key => $value ) {
      
      if ( property_exists( $this, $key ) )
        $this->$key = $value;
      
    }
    
    $this->db = new \Memcached();
    $this->db->addServer( $config['host'], $config['port'] );
    
  }
  
  public function save( $data ) {
    
    $id = PhoneticPassword::getPassword( $this->keylength );
    
    // fails if key exists, tries again by recursively calling itself
    // might ran out of stack space
    if ( !$this->db->add( $this->prefix . $id, $data ) )
      return $this->save( $data );
    
    return $id;
    
  }
  
  public function get( $id ) {
    return $this->db->get( $this->prefix . $id );
  }
  
  public function delete( $id ) {
    return $this->db->delete( $this->prefix . $id );
  }
  
}

class Debug {
  protected static $logemails = array();
  protected static $instance;
  
  public function __construct( $config ) {
    self::$logemails = $config['logemails'];
    self::$instance  = $this;
  }
  
  public function setupErrorhandler() {
    
    $callback = array( $this, 'commonErrorHandler');
    
    set_error_handler( $callback );
    set_exception_handler( $callback );
    register_shutdown_function( array( $this, 'checkLastError') );
    
  }
  
  public function checkLastError() {
    
    $error = error_get_last();
    
    if ( $error and $error['type'] == E_ERROR ) {
      
      $subject = '[EZP] Fatal error: ' . $error['message'];
      $body    =
        $error['message'] . "\n" .
        "  in file " . $error['file'] . ':' . $error['line'] . "\n\n"
      ;
      
      $body .= self::getRequestInformation( 2 );
      
      foreach( self::$logemails as $email )
        mail( $email, $subject, $body );
      
    }
    
  }
  
  public function commonErrorHandler( $errno, $errstr  = null, $errfile  = null, $errline  = null, $errcontext = null ) {
    
    // error suppression operator @ sets error_reporting to 0
    // per manual
    if ( ini_get('error_reporting') == 0 )
      return true;
    
    $die       = false;
    $skipecho  = false;
    $exception = false;
    
    if ( func_num_args() == 5) {
      // not an exception
      list( $errno, $errstr, $errfile, $errline ) = func_get_args();
      $backtrace = debug_backtrace();
      array_shift( $backtrace ); // shift the array forward, removing the call to this function
      
      switch ( $errno ) {
        case E_USER_ERROR:        $class = 'E_USER_ERROR';        $die = true; break;
        case E_USER_WARNING:      $class = 'E_USER_WARNING';      break;
        case E_USER_NOTICE:       $class = 'E_USER_NOTICE';       $die = true; break;
        case E_USER_DEPRECATED:   $class = 'E_USER_DEPRECATED';   $skipecho = true; break;
        case E_PARSE:             $class = 'E_PARSE';             break;
        case E_NOTICE:            $class = 'E_NOTICE';            $skipecho = true; break;
        case E_ERROR:             $class = 'E_ERROR';             break;
        case E_WARNING:           $class = 'E_WARNING';           break;
        case E_RECOVERABLE_ERROR: $class = 'E_RECOVERABLE_ERROR'; break;
        case E_DEPRECATED:        $class = 'E_DEPRECATED';        $skipecho = true; break;
        case E_STRICT:            $class = 'E_STRICT';            $skipecho = true; break;
        default:                  $class = "E_IDONTKNOW";         break;
      }
      
    } else {
      
      $exception = func_get_arg(0);
      $errno     = $exception->getCode();
      $errstr    = str_replace( array("\r\n", "\n", "\r"), '', $exception->getMessage() );
      $errfile   = $exception->getFile();
      $errline   = $exception->getLine();
      $backtrace = $exception->getTrace();
      $class     = 'CAUGHT ' . get_class( $exception ) . ' EXCEPTION';
      $die       = true;
      
    }
    
    $errfile = str_replace( '\\', '/', $errfile );
    $subject =
      "$class (#$errno)\n" .
      "  $errstr\n" .
      "  in file $errfile:$errline\n"
    ;
    
    $error = $subject . "\n" . self::formatBacktrace( $backtrace );
    $error = trim( $error ) . "\n\n" . self::getRequestInformation( 2 );
    
    if ( defined('PRODUCTION') and !PRODUCTION )
      echo "<pre style=\"background-color: #fff; color: #666;\">\n", $error, "</pre>\n";
    else {
      
      $subject = str_replace( "\n", "", $subject );
      foreach( self::$logemails as $email )
        mail( $email, $subject, $body );
      
    }
    
    if ( $die )
      exit( 1 );
    
    return true;
    
  }
  
  public static function stringifyVariable( $arg ) {
    
    ob_start();
    
    if ( is_object( $arg ) )
      echo "\\", get_class( $arg );
    else
      var_dump( $arg );
    
    $dump = trim( ob_get_clean() );
    
    if ( strlen( $dump ) > 100 )
      return substr( $dump, 0, 100 ) . '...';
    
    return $dump;
    
  }

  public static function formatBacktrace( $backtrace, $skipfirstargs = false ) {
    
    $ret = '';
    
    // format backtrace
    // stolen and rewritten from php.net documentation comments
    foreach ( $backtrace as $k => $v ) {
      
      if ( !@$v['file'] )
        $v['file'] = '{UNKNOWNFILE}';
      
      if ( !@$v['line'] )
        $v['line'] = '{UNKNOWNLINE}';
      
      if ( isset( $v['class'] ) )
        $trace = '  ' . $k . ' ' . $v['class'] . $v['type'] . $v['function'] . '(';
      elseif ( isset( $v['function'] ) )
        $trace = '  ' . $k . ' ' . $v['function'] . '(';
      
      if ( !empty( $v['args'] ) and ( ( $skipfirstargs == true and $k != 0 ) or !$skipfirstargs ) ) {
        
        $separator = '';
        foreach( $v['args'] as $arg ) {
          
          $trace .= $separator . self::stringifyVariable( $arg );
          $separator = ', ';
          
        }
        
      }
      
      $v['file'] = str_replace( DIRECTORY_SEPARATOR, '/', $v['file'] );
      
      $ret .=
        "\n" .
        $trace . ") --- " . $v['file'] . ':' . $v['line'];
      
    }
    
    return ltrim( $ret . "\n", "\r\n" );
    
  }
  
  public static function getRequestInformation( $indent = 0 ) {
    
    $out =
      "REQUEST_URI:          " . @$_SERVER['REQUEST_URI'] . "\n" .
      "SERVER_NAME:          " . @$_SERVER['SERVER_NAME'] . "\n" .
      "REFERER:              " . @$_SERVER['HTTP_REFERER'] . "\n\n" .
      "USER ----------------------------------------\n" .
      "HTTP_USER_AGENT:      " . @$_SERVER['HTTP_USER_AGENT'] . "\n" .
      "REMOTE_ADDR:          " . @$_SERVER['REMOTE_ADDR'] . "\n" .
      "REMOTE_HOST:          " . @$_SERVER['REMOTE_HOST'] . "\n" .
      "HTTP_ACCEPT:          " . @$_SERVER['HTTP_ACCEPT'] . "\n" .
      "HTTP_ACCEPT_CHARSET:  " . @$_SERVER['HTTP_ACCEPT_CHARSET'] . "\n" .
      "HTTP_ACCEPT_ENCODING: " . @$_SERVER['HTTP_ACCEPT_ENCODING'] . "\n" .
      "HTTP_ACCEPT_LANGUAGE: " . @$_SERVER['HTTP_ACCEPT_LANGUAGE'] . "\n" .
      "HTTP_CACHE_CONTROL:   " . @$_SERVER['HTTP_CACHE_CONTROL'] . "\n" .
      "HTTP_HOST:            " . @$_SERVER['HTTP_HOST'] . "\n" .
      "HTTP_VIA:             " . @$_SERVER['HTTP_VIA'] . "\n" .
      "HTTP_X_FORWARDED_FOR: " . @$_SERVER['HTTP_X_FORWARDED_FOR'] . "\n" .
      "\n" .
      ( $_GET?
       "GET: ----------------------------------------\n" .
        var_export( $_GET, true ) . "\n"    : "GET:    empty" ) . "\n" .
      ( $_POST?
       "\nPOST: ----------------------------------------\n" .
        var_export( $_POST, true ) . "\n"   : "POST:   empty" ) . "\n" .
      ( $_FILES?
       "\nFILES: ----------------------------------------\n" .
        var_export( $_FILES, true ) . "\n"  : "FILES:  empty" ) . "\n" .
      ( $_COOKIE?
       "\nCOOKIE: ----------------------------------------\n" .
        var_export( $_COOKIE, true ) . "\n" : "COOKIE: empty" ) . "\n"
    ;
    
    if ( $indent )
      $out = preg_replace(
        '/^/msUi', str_repeat( ' ', $indent ), $out
      );
    
    return $out;
    
  }
  
}

class Template {
  protected $vars;
  
  public function render( $template, $return = false ) {
    
    extract( $this->vars );
    
    if ( $return )
      ob_start();
    
    include $template;
    
    if ( $return )
      return ob_end_clean();
    else
      die();
    
  }
  
  public function __set( $key, $value ) {
    $this->vars[ $key ] = $value;
  }
  
  public function __get( $key ) {
    return isset( $this->vars[ $key ] )? $this->vars[ $key ]: null;
  }
  
}

function jsonOutput( $data ) {
  
  if ( !headers_sent() )
    header('Content-Type: application/json; charset=UTF-8');
  
  echo json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT );
  
}

function redirect( $url, $ispermanent = false, $die = true ) {
  
  if ( headers_sent() )
    return false;
  
  $code = $ispermanent? 301: 302;
  header("Location: $url", true, $code );
  
  if ( $die )
    die();
  
}

function esc( $what, $type ) {
  
  switch ( $type ) {
    case 'html':
    case 'attr':
      return htmlspecialchars( $what, ENT_QUOTES, 'UTF-8', true );
      break;
    case 'url':
      return rawurlencode( $what );
      break;
    case 'json':
      return json_encode( $what, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT );
      break;
    default:
      throw new Exception("Unknown escape type: $type");
      break;
  }
  
}