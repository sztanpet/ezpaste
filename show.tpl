<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>EZP</title>
    <meta name="description" content="Simple fast and easy pastebin service">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="style.css">
    <script src="jquery-1.9.1.min.js"></script>
    <script src="client.js"></script>
  </head>
  <body>
    <div id="wrap">
      <div id="header">
        EZ Paste, just paste your shit and press CTRL+S. We tried to copy the URL to your clipboard, press the button to try again: <button id="copyurl" data-clipboard-text="<?=$fullurl?>">Copy URL</button>
      </div>
      <ol id="paste">
        <?php foreach( $data as $line ): ?>
          <li><?=esc( $line, 'html' ); ?></li>
        <?php endforeach; ?>
      </ol>
    </div>
  </body>
</html>