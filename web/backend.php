<?php

define('BASE_PATH', realpath( __DIR__ . '/..' ) . '/' );
if ( isset( $_SERVER['APPLICATION_ENV'] ) and $_SERVER['APPLICATION_ENV'] == 'developer' )
  define('PRODUCTION', false );
else
  define('PRODUCTION', true );

include( BASE_PATH . 'utils.php' );
$action = isset( $_REQUEST['action'] )? $_REQUEST['action']: redirect('/');
$config = include( BASE_PATH . 'config.php' );
$debug  = new Debug( $config );
$debug->setupErrorhandler();

switch( $action ) {
  
  case 'show':
    
    $id = isset( $_REQUEST['id'] )? $_REQUEST['id']: redirect('/');
    if ( !preg_match('/^[a-z0-9]+$/', $id ) )
      return;
    
    $db   = new DB( $config );
    $data = $db->get( $id );
    
    if ( !$data )
      redirect('/');
    
    $tpl = new Template();
    $tpl->data    = explode( "\n", str_replace( array("\r\n", "\r",), "\n", $data ) );
    $tpl->fullurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $tpl->render( BASE_PATH . 'show.tpl' );
    break;
  
  case 'save':
    
    if ( empty( $_POST ) or !isset( $_POST['data'] ) )
      redirect('/');
    
    $db = new DB( $config );
    $id = $db->save( (string)$_POST['data'] );
    redirect("/$id");
    break;
  
  default:
    redirect('/');
    break;
  
}